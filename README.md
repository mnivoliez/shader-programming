# shader-programming

This repo will handle just enough code to create basic shape on which shaders will be apply.

# Design

```plantuml
class Vertex {
 +glm::vec3 Position
 +glm::vec3 Normal
 +glm::vec2 TexCoords
}

class Texture {
 +unsigned int id
 +string type
}

class Mesh {
 +vector<Vertex> vertices
 +vector<unsigned int> indices
 +vector<Texture> textures
 -unsigned int VAO
 -unsigned int VBO
 -unsigned int EBO
}

Mesh *-- "0..n" Vertex
Mesh *-- "0..n" Texture

class Model

Model *-- "0..n" Mesh

class Shader

abstract class Material {
  +Shader *shader;
}

Material "0..n" *-- "1" Shader

class BaseMaterial {
	+glm::vec3 diffuse
	+glm::vec3 specular
	+glm::vec3 anbiant
}

Material <|-- BaseMaterial

class MeshFactory {
  +Mesh generate_cube (float size_x, float size_y, float size_z)
  +Mesh generate_sphere(float radius, int slice, stack)
}

class RenderManager {
    +void RenderModels(std::vector<*Model> models, std::vector<*Light> lights)
}

class Renderer {
    +void Prepare(Shader *shader, std::vector<*Light> lights)
    +void RenderMesh(Mesh *mesh)
}

RenderManager *-- "0..n" Renderer

abstract class Light {
    glm::vec3 ambiant
    glm::vec3 diffuse
    glm::vec3 specular
}

class DirectionalLight {
    +glm:vec3 direction
}

class PointLight {
    +glm:vec3 position
    +float constant
    +float linear
    +float quadratic 
}

class SpotLight {
    +glm:vec3  position
    +glm:vec3  direction
    +float cutOff
}

Light <|-- DirectionalLight
Light <|-- PointLight
Light <|-- SpotLight

```
