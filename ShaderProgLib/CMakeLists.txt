cmake_minimum_required(VERSION 3.12)

set (LIB_SOURCES
  src/lib.cpp
  src/Mesh.cpp
)

set(LIBRARY_NAME ShaderProgLib)
add_library(${LIBRARY_NAME} SHARED ${LIB_SOURCES})
target_link_libraries(${LIBRARY_NAME}
  PUBLIC 
    glm
    glad
  PRIVATE
    ${CMAKE_DL_LIBS}
)

target_include_directories(${LIBRARY_NAME}  
  PUBLIC   
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>   
    $<INSTALL_INTERFACE:include>   
  PRIVATE 
    src
)

set_target_properties(${LIBRARY_NAME} PROPERTIES LINKER_LANGUAGE CXX)
