#pragma once 

#include "Vertex.hpp"
#include "Texture.hpp"

#include <vector>

class Mesh {
  public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    Mesh(std::vector<Vertex>, std::vector<unsigned int>, std::vector<Texture>);

  private:
    unsigned int VAO, VBO, EBO;

    void SetupMesh();
};
