#pragma once

#include "Mesh.hpp"

namespace ShaderLib {
  Mesh generate_cube(const float size_x, const float size_y, const float size_z);

  Mesh generate_uv_sphere(const float radius, const int meridians, const int parallels);
}
